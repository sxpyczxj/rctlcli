/*
 * =====================================================================================
 *
 *       Filename:  sysdef.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年01月13日 14时33分50秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  jianxi sun (jianxi), ycsunjane@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef __SYSDEF_H__
#define __SYSDEF_H__

/* rctl system command */
#define RCTLBASH 	"rctlbash"

#define BASHPORT 	(6001)
#define CMDLEN (2048)
#define BUFLEN (2048)

#define DEVID_LEN 	(33)
#define ETH_ALEN 	(6)
#define MACSTR 		(18)

struct reg_t {
	char  class[DEVID_LEN];
	unsigned char  mac[ETH_ALEN];
}__attribute__((packed));

#endif /* __SYSDEF_H__ */
