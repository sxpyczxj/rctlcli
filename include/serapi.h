/*
 * =====================================================================================
 *
 *       Filename:  serapi.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年01月17日 21时34分52秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  jianxi sun (jianxi), ycsunjane@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef __SERAPI_H__
#define __SERAPI_H__
int Pthread_create(void *(*start_routine) (void *), void *arg);
#endif /*__SERAPI_H__*/
