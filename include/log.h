/*
 * =====================================================================================
 *
 *       Filename:  log.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年07月25日 10时13分27秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  jianxi sun (jianxi), ycsunjane@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef __LOG_H__
#define __LOG_H__

#include <syslog.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

extern int debug;
extern int daemon_enable;

#define __R_FILE__  ((strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__))

#define __sys_log(LEVEL, fmt, ...) 	\
	do { 				\
		if(LEVEL <= debug) { 	\
			if(daemon_enable) { 	\
				syslog(LEVEL|LOG_USER, "(%lu) %s +%d %s(): "fmt, 	\
					(unsigned long)pthread_self(), 			\
					__R_FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
			} else { 							\
				fprintf(stderr, "(%lu) %s +%d %s(): "fmt, 		\
					(unsigned long)pthread_self(),  		\
					__R_FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
			} 	\
		} 		\
	} while(0)

#define sys_err(fmt, ...) 	__sys_log(LOG_ERR, "ERR: "fmt, ##__VA_ARGS__)
#define sys_warn(fmt, ...) 	__sys_log(LOG_WARNING, "WARNING: "fmt, ##__VA_ARGS__)
#define sys_debug(fmt, ...) 	__sys_log(LOG_DEBUG, "DEBUG: "fmt, ##__VA_ARGS__)

#define __sys_log2(fmt, ...) 				\
	do { 						\
		fprintf(stderr, fmt,  ##__VA_ARGS__); 	\
	} while(0)
#define sys_info(fmt, ...) 	__sys_log2(fmt, ##__VA_ARGS__)

#define panic() 		\
do { 				\
	sys_err("Panic\n"); 	\
	exit(-1); 		\
} while(0)

#endif /* __LOG_H__ */
