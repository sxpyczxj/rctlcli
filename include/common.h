/*
 * =====================================================================================
 *
 *       Filename:  common.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年01月15日 16时03分44秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  jianxi sun (jianxi), ycsunjane@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef __COMMON_H__
#define __COMMON_H__
#include "api.h"
#include "log.h"
#include "sysdef.h"
#include "ssltcp.h"
#include "exchange.h"

#define MACSTR_LEN 	(18)
void fllush_stdin();
char *getmacstr(unsigned char *mac);
int tcp_alive(int sock);
#endif /* __COMMON_H__ */
