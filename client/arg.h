/*
 * =====================================================================================
 *
 *       Filename:  arg.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年01月15日 16时22分37秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  jianxi sun (jianxi), ycsunjane@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef __ARG_H__
#define __ARG_H__
#include "cmdline.h"

extern struct  gengetopt_args_info args_info;
void proc_args(int argc, char *argv[]);
#endif /* __ARG_H__ */
