/*
 * =====================================================================================
 *
 *       Filename:  bash.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年04月07日 15时15分15秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  jianxi sun (jianxi), ycsunjane@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef __BASH_H__
#define __BASH_H__

void bashfrom();
#endif /* __BASH_H__ */
